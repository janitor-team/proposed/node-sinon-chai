# Change log

## 3.4.0

### Features
* Add support for sinon 8 ([#143](https://github.com/domenic/sinon-chai/issues/143))
